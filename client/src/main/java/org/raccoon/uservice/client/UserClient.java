package org.raccoon.uservice.client;

import io.grpc.ManagedChannel;
import org.raccoon.uservice.client.grpc.UserListProto;
import org.raccoon.uservice.client.grpc.UserServiceGrpc;
import org.raccoon.uservice.client.grpc.Void;

import static org.raccoon.uservice.client.grpc.UserServiceGrpc.newBlockingStub;

public class UserClient extends AbstractGrpcClient {

    private final UserServiceGrpc.UserServiceBlockingStub stub;

    UserClient(String host, int port) {
        super(host, port);
        this.stub = newBlockingStub(this.channel);
    }

    UserClient(ManagedChannel channel) {
        super(channel);
        this.stub = newBlockingStub(channel);
    }

    public UserListProto getAllUsers() {
        return stub.getAllUsers(Void.newBuilder().build());
    }

}
