package org.raccoon.uservice.client;

import io.grpc.ManagedChannel;
import io.grpc.netty.NettyChannelBuilder;

abstract class AbstractGrpcClient implements AutoCloseable {

    protected final ManagedChannel channel;

    private final boolean sharedChannel;

    AbstractGrpcClient(String host, int port) {
        this.channel = NettyChannelBuilder.forAddress(host, port)
                .usePlaintext()
                .build();
        this.sharedChannel = false;
    }

    AbstractGrpcClient(ManagedChannel channel) {
        this.channel = channel;
        this.sharedChannel = true;
    }

    @Override
    public void close() throws Exception {
        if (!sharedChannel) {
            channel.shutdown();
        }
    }

}
