# UService
[![Pipeline](https://gitlab.com/raccoon1515/uservice/badges/master/pipeline.svg)](https://gitlab.com/raccoon1515/uservice/-/commits/master)
[![Coverage](https://gitlab.com/raccoon1515/uservice/badges/master/coverage.svg)](https://gitlab.com/raccoon1515/uservice/-/commits/master)

A simple JWT authorization service. Supports user storage and CRUD operations on them. 

### Build from sources
```bash
$ ./gradlew clean :service:build -x test
```

### Key generation
...key_path - absolute path to key, assigned in application.properties
```bash
$ export priv_key_path=/etc/uservice/keys/private.pem
$ export pub_key_path=/etc/uservice/keys/public.pem
$ mkdir -p /etc/uservice/keys

$ openssl genrsa -out key.pem 2048 2> /dev/null
$ openssl pkcs8 -topk8 -inform PEM -outform PEM -in key.pem -out ${priv_key_path} -nocrypt
$ openssl rsa -in key.pem -pubout -outform PEM -out ${pub_key_path} 2> /dev/null
$ rm -f key.pem || true
```

### Run UService
```bash
$ java -jar build/service/libs/uservice-service-*.jar
```

## Configuration
##### Logging configuration
service/src/main/resources/log4j2.xml

##### Application configuration
service/src/main/resources/application.properties

## OpenAPI
https://app.swaggerhub.com/apis/202b/Uservice/0.0.1

## Notes
You need to create directory `/var/log/uservice` and
grant access to your user to created directory. 
 
This will be fixed next time.

## Authentication diagram
![AuthDiagram](doc/AuthDiagram.png "Simple authentication diagram")

## Docker
### Сборка и пуш образа
**Сборка и пуш образа в Gitlab registry:**

Для пуша необходимо предоставить креденшалы для входа в registry. 
Для этого нужно:
```bash
$ docker login registry.gitlab.com
```

После этого можно собирать и пушить:
```bash
$ ./gradlew clean :service:jib
```

Или указать креденшалы в аргументах gradle-скрипта:
```bash
$ /gradlew clean :service:jib -PregistryLogin=<логин> -PregistryPass=<пароль>
```


**Сборка образа:**
```bash
$ ./gradlew clean :service:jibDockerBuild
```
    
**Сборка tarball:**
```bash
$ ./gradlew clean :service:jibBuildTar
```
    
**Собрать образ из tarball:**
```bash
$ docker load --input build/server/docker/uservice_<версия>.tar
```

**Пуш образа в Gitlab registry:**
```bash
$ docker push registry.gitlab.com/raccoon1515/uservice:<тег версии>
```

### Использование образа
Образ не содержит в себе ключи, поэтому они должны быть сгенерированы
вручную и прокинуты через volume по пути, указанному в
_application.properties_ (`/etc/uservice/keys/` по умолчанию)
(Когда-нибудь будет лучше вариант)

**Запуск контейнера:**
```bash
$ docker run --name uservice \
             -p <порт на который замапить сервис>:1337
             -e MYSQL_HOST=<хост бд>
             -v "<путь до директории со сгенерированными pem-ключами>:/etc/uservice/keys" \
             -v "<путь до директории хранения логов>:/var/log/uservice" \
             -v "<путь для изменения настроек приложения>:/etc/uservice/resources/application.properties" \
             -v "<путь для изменения настроек логирования>:/etc/uservice/resources/log4j2.xml" \
             registry.gitlab.com/raccoon1515/uservice:<тег версии>
```

**Переменные среды контейнера:**
- _MYSQL_HOST_: хост бд (по умолчанию localhost),
- _MYSQL_PORT_: порт бд (по умолчанию 3306),
- _AC_TOKEN_LT_: время жизни access-токена в минутах (по умолчанию 15 минут),
- _RF_TOKEN_LT_: время жизни refresh-токена в минутах (по умолчанию 43200 минут),
- _CACHE_USERS_TTL_: TTL кэша пользователей в минутах (по умолчанию 50 минут),
- (TODO) _HTTP_PORT_: порт HTTP сервера (по умолчанию 1337),
- (TODO) _GRPC_PORT_: порт Grpc сервера (по умолчанию 1338).
