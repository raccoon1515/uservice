const hostUrl = 'http://localhost:1337';

export const environment = {
    production: true,
    apiUrl: hostUrl + '/admin',
};
