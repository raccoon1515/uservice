import {Component} from '@angular/core';
import {MatDialog} from "@angular/material/dialog";
import {LoginFormComponent} from "./login-form/login-form.component";
import {AuthService} from "./auth.service";

@Component({
    selector: 'app-root',
    templateUrl: './app.component.html',
    styleUrls: ['./app.component.scss']
})
export class AppComponent {

    constructor(private dialog: MatDialog,
                private authService: AuthService) {
    }

    private openLoginDialog() {
        this.dialog.open(LoginFormComponent)
    }

}
