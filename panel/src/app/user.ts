export interface User {
    id?: number | null;
    username: string;
    password: string;
    enabled: boolean;
    customFields: Array<UserFieldValue>
}

export interface UserFieldValue {
    id?: number | null;
    value: string;
    type: UserField;
}

export interface UserField {
    id?: number | null;
    type: string;
    name: string;
    defaultValue?: string | null;
}
