import {Component, Inject, OnInit} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from "@angular/material/dialog";
import {UserField} from "../user";
import {EditAction, EditEvent} from "../user-list/user-list.component";

@Component({
    selector: 'app-field-edit',
    templateUrl: './field-edit.component.html',
    styleUrls: ['./field-edit.component.scss']
})
export class FieldEditComponent implements OnInit {

    private editingName: string;

    constructor(private dialogRef: MatDialogRef<FieldEditComponent>,
                @Inject(MAT_DIALOG_DATA) public field: UserField) {
    }

    ngOnInit(): void {
        this.editingName = this.field.name;
    }

    private onCancelClick(): void {
        this.dialogRef.close();
    }

    private closeWithUpdate(): EditEvent<UserField> {
        return {
            item: this.field,
            action: EditAction.UPDATE
        }
    }

    private closeWithDelete(): EditEvent<UserField> {
        return {
            item: this.field,
            action: EditAction.DELETE
        }
    }

}
