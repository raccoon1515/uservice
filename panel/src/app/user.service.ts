import {Injectable} from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {User} from "./user";
import {Observable} from "rxjs";
import {environment} from "../environments/environment";

@Injectable({
    providedIn: 'root'
})
export class UserService {
    private USER_EP = environment.apiUrl + '/user';

    constructor(private http: HttpClient) {
    }

    public getUsers(): Observable<Array<User>> {
        return this.http.get<Array<User>>(this.USER_EP);
    }

    public createUser(user: User): Observable<User> {
        return this.http.post<User>(this.USER_EP, user);
    }

    public updateUser(user: User): Observable<User> {
        return this.http.put<User>(this.USER_EP, user);
    }

    public deleteUser(user: User): Observable<User> {
        return this.http.delete<User>(this.USER_EP + '/' + user.id);
    }

}
