import {BrowserModule} from '@angular/platform-browser';
import {ErrorHandler, NgModule} from '@angular/core';
import {AppComponent} from './app.component';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {UserListComponent} from './user-list/user-list.component';
import {MatTableModule} from "@angular/material/table";
import {MatToolbarModule} from "@angular/material/toolbar";
import {RouterModule, Routes} from "@angular/router";
import {HTTP_INTERCEPTORS, HttpClientModule} from "@angular/common/http";
import {UserCreationFormComponent} from './user-creation-form/user-creation-form.component';
import {MatExpansionModule} from "@angular/material/expansion";
import {MatFormFieldModule} from "@angular/material/form-field";
import {MatInputModule} from "@angular/material/input";
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import {MatButtonModule} from "@angular/material/button";
import {UserEditComponent} from './user-edit/user-edit.component';
import {MatDialogModule} from "@angular/material/dialog";
import {MatSelectModule} from "@angular/material/select";
import {LoginFormComponent} from './login-form/login-form.component';
import {AuthInterceptor} from "./auth.interceptor";
import {GlobalErrorHandler} from "./error.handler";
import {MatSnackBarModule} from "@angular/material/snack-bar";
import {MatSlideToggleModule} from "@angular/material/slide-toggle";
import { FieldCreationFormComponent } from './field-creation-form/field-creation-form.component';
import { FieldEditComponent } from './field-edit/field-edit.component';

const appRoutes: Routes = [
    {path: 'users', component: UserListComponent},
];

@NgModule({
    declarations: [
        AppComponent,
        UserListComponent,
        UserCreationFormComponent,
        UserEditComponent,
        LoginFormComponent,
        FieldCreationFormComponent,
        FieldEditComponent,
    ],
    entryComponents: [
        UserEditComponent,
        LoginFormComponent,
        FieldEditComponent,
    ],
    imports: [
        RouterModule.forRoot(appRoutes),
        BrowserModule,
        BrowserAnimationsModule,
        MatTableModule,
        MatToolbarModule,
        HttpClientModule,
        MatExpansionModule,
        MatFormFieldModule,
        MatInputModule,
        FormsModule,
        MatButtonModule,
        MatDialogModule,
        MatSelectModule,
        ReactiveFormsModule,
        MatSnackBarModule,
        MatSlideToggleModule,
    ],
    providers: [
        {
            provide: HTTP_INTERCEPTORS,
            useClass: AuthInterceptor,
            multi: true
        },
        {
            provide: ErrorHandler,
            useClass: GlobalErrorHandler
        }],
    bootstrap: [AppComponent]
})
export class AppModule {
}
