import {Injectable} from '@angular/core';
import {environment} from "../environments/environment";
import {HttpClient, HttpHeaders} from "@angular/common/http";
import {Observable} from "rxjs";
import {NotificationService} from "./notification.service";

interface LoginResponse {
    accessToken: string,
    refreshToken: string
}

@Injectable({
    providedIn: 'root'
})
export class AuthService {

    private AUTH_EP = environment.apiUrl + '/auth';

    constructor(private http: HttpClient,
                private notificationService: NotificationService) {
    }

    public login(credentials: { username: String; password: String }) {
        this.http.post<LoginResponse>(this.AUTH_EP + '/signin', credentials)
            .subscribe(response => {
                this.storeTokens(response);
                this.notificationService.showSuccess("Welcome!");
            });
    }

    public storeTokens(response: LoginResponse) {
        localStorage.setItem('AT', response.accessToken);
        localStorage.setItem('RT', response.refreshToken);
    }

    public refreshToken(): Observable<LoginResponse> {
        const refreshToken = localStorage.getItem('RT');
        const headers = new HttpHeaders({
            'Authorization': 'Bearer ' + refreshToken
        });
        return this.http.post<LoginResponse>(this.AUTH_EP + '/refresh', {}, {headers: headers});
    }

    public logout(): void {
        this.http.post(this.AUTH_EP + '/signout', {}).subscribe(res => {
            localStorage.removeItem('AT');
            localStorage.removeItem('RT');
            this.notificationService.showSuccess("Good Bye!");
        })
    }

    public isAuthenticated(): boolean {
        return !!(localStorage.getItem('AT') && localStorage.getItem('RT'));
    }

}
