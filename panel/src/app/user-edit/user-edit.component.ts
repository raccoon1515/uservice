import {Component, Inject, OnInit} from '@angular/core';
import {User} from "../user";
import {MAT_DIALOG_DATA, MatDialogRef} from "@angular/material/dialog";
import {FieldService} from "../field.service";
import {EditAction, EditEvent} from "../user-list/user-list.component";

@Component({
    selector: 'app-user-edit',
    templateUrl: './user-edit.component.html',
    styleUrls: ['./user-edit.component.scss']
})
export class UserEditComponent implements OnInit {

    private editingUsername: string;

    constructor(private fieldService: FieldService,
                private dialogRef: MatDialogRef<UserEditComponent>,
                @Inject(MAT_DIALOG_DATA) public user: User) {
    }

    ngOnInit(): void {
        this.fieldService.getAllFields().subscribe(fields => {
            fields.filter(field => this.user.customFields.findIndex(value => field.name === value.type.name) === -1)
                .forEach(value => this.user.customFields.push({
                    type: value,
                    value: ''
                }));
        });
        this.editingUsername = this.user.username;
    }

    private onCancelClick(): void {
        this.dialogRef.close();
    }

    private closeWithUpdate(): EditEvent<User> {
        return {
            item: this.user,
            action: EditAction.UPDATE
        }
    }

    private closeWithDelete(): EditEvent<User> {
        return {
            item: this.user,
            action: EditAction.DELETE
        }
    }

}
