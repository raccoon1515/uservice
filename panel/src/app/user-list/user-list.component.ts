import {Component, OnInit, ViewChild} from '@angular/core';
import {User, UserField} from "../user";
import {UserService} from "../user.service";
import {MatTable} from "@angular/material/table";
import {UserEditComponent} from "../user-edit/user-edit.component";
import {MatDialog} from "@angular/material/dialog";
import {FieldService} from "../field.service";
import {NotificationService} from "../notification.service";
import {FieldEditComponent} from "../field-edit/field-edit.component";

@Component({
    selector: 'app-user-list',
    templateUrl: './user-list.component.html',
    styleUrls: ['./user-list.component.scss']
})
export class UserListComponent implements OnInit {
    private users: Array<User> = [];
    private displayedColumns: string[] = ['id', 'username'];
    private customFields: Array<UserField>;
    @ViewChild(MatTable, {static: false}) table: MatTable<any>;

    constructor(private userService: UserService,
                private notificationService: NotificationService,
                private fieldService: FieldService,
                private dialog: MatDialog) {
    }

    ngOnInit(): void {
        this.fieldService.getAllFields().subscribe(fields => {
            this.customFields = fields;
            fields.forEach(field => this.displayedColumns.push(field.name));
        });
        this.userService.getUsers().subscribe((users: Array<User>) => {
            this.users = users;
        });
    }

    private openEditFieldDialog(editableField: UserField): void {
        this.dialog
            .open(FieldEditComponent, {
                data: UserListComponent.copyObject(editableField)
            })
            .afterClosed().subscribe((editEvent: EditEvent<UserField>) => {
                if (editEvent) {
                    const item = editEvent.item;
                    if (editEvent.action == EditAction.UPDATE) {
                        this.updateField(item);
                    } else if (editEvent.action == EditAction.DELETE) {
                        this.deleteField(item);
                    }
                }
            }
        );
    }

    private updateField(updatingField: UserField) {
        this.fieldService.updateField(updatingField).subscribe(updatedField => {
            this.notificationService.showSuccess('Field ' + updatedField.name + ' updated.');
        });
    }

    private deleteField(removingField: UserField) {
        this.fieldService.deleteField(removingField).subscribe(() =>
            this.notificationService.showSuccess('Field ' + removingField.name + ' removed.'));
    }

    addDisplayUser(user: User): void {
        this.users.push(user);
        this.refreshTable();
    }

    private openEditUserDialog(editableUser: User): void {
        this.dialog
            .open(UserEditComponent, {
                data: UserListComponent.copyObject(editableUser)
            })
            .afterClosed().subscribe((editEvent: EditEvent<User>) => {
                if (editEvent) {
                    const item = editEvent.item;
                    if (editEvent.action == EditAction.UPDATE) {
                        this.updateUser(item);
                    } else if (editEvent.action == EditAction.DELETE) {
                        this.deleteUser(item);
                    }
                }
            }
        );
    }

    private updateUser(updatingUser: User): void {
        this.userService.updateUser(updatingUser).subscribe((updatedUser: User) => {
            this.notificationService.showSuccess('User ' + updatingUser.username + 'updated.');
            const userArrayIndex = this.users.findIndex(user => user.id == updatingUser.id);
            this.users[userArrayIndex] = updatedUser;
            this.refreshTable();
        })
    }

    private deleteUser(deletingUser: User): void {
        this.userService.deleteUser(deletingUser).subscribe(() => {
            this.notificationService.showSuccess('User ' + deletingUser.username + 'deleted.');
            const userArrayIndex = this.users.indexOf(deletingUser);
            this.users.splice(userArrayIndex, 1);
            this.refreshTable();
        });
    }

    private refreshTable(): void {
        this.table.renderRows();
    }

    private static copyObject<T>(object: T): T {
        return JSON.parse(JSON.stringify(object))
    }
}

export interface EditEvent<T> {
    action: EditAction,
    item: T
}

export enum EditAction {
    DELETE,
    UPDATE,
}

