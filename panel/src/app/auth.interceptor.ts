import {HttpClient, HttpEvent, HttpHandler, HttpInterceptor, HttpRequest} from "@angular/common/http";
import {Injectable} from "@angular/core";
import {Observable, Subscriber} from "rxjs";
import {AuthService} from "./auth.service";


type CallerRequest = {
    subscriber: Subscriber<any>;
    failedRequest: HttpRequest<any>;
};

@Injectable()
export class AuthInterceptor implements HttpInterceptor {
    private isRefreshingToken = false;
    private requests: CallerRequest[] = [];

    constructor(public authService: AuthService, public http: HttpClient) { }

    intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        const accessToken = localStorage.getItem('AT');
        if (accessToken && !this.isRefreshingToken) {
            req = req.clone({
                setHeaders: {
                    'Authorization': 'Bearer ' + accessToken
                }
            })
        }

        return new Observable<HttpEvent<any>>(subscriber => {
            const originalRequestSubscription = next.handle(req)
                .subscribe(
                    (response) => subscriber.next(response),
                    (error) => {
                        if (error.status === 401) {
                            this.handleUnauthorizedError(subscriber, req);
                        } else {
                            subscriber.error(error);
                        }
                    },
                    () => subscriber.complete());
            return () => originalRequestSubscription.unsubscribe();
        });
    }

    private handleUnauthorizedError(subscriber: Subscriber<HttpEvent<any>>, request: HttpRequest<any>) {
        this.requests.push({subscriber, failedRequest: request});
        if (!this.isRefreshingToken) {
            this.isRefreshingToken = true;
            this.authService.refreshToken().subscribe(response => {
                this.authService.storeTokens(response);
                this.isRefreshingToken = false;
                this.repeatFailedRequests(response.accessToken)
            }, this.authService.logout)
        }
    }

    private repeatFailedRequests(accessToken: string) {
        this.requests.forEach((c) => {
            const requestWithNewToken = c.failedRequest.clone({
                headers: c.failedRequest.headers.set('Authorization', 'Bearer ' + accessToken)
            });
            this.repeatRequest(requestWithNewToken, c.subscriber);
        });
        this.requests = [];
    }

    private repeatRequest(requestWithNewToken: HttpRequest<any>, subscriber: Subscriber<any>) {
        this.http.request(requestWithNewToken)
            .subscribe((res) => subscriber.next(res),
                (err) => {
                    if (err.status === 401) {
                        this.authService.logout();
                    }
                    subscriber.error(err);
                },
                () => subscriber.complete());
    }

}
