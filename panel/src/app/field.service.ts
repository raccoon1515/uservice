import {Injectable} from '@angular/core';
import {environment} from "../environments/environment";
import {HttpClient} from "@angular/common/http";
import {UserField} from "./user";
import {Observable} from "rxjs";

@Injectable({
    providedIn: 'root'
})
export class FieldService {

    private FIELD_EP = environment.apiUrl + '/field';

    constructor(private http: HttpClient) {
    }

    public getAllFieldTypes(): Observable<Array<string>> {
        return this.http.get<Array<string>>(this.FIELD_EP + '/types');
    }

    public getAllFields(): Observable<Array<UserField>> {
        return this.http.get<Array<UserField>>(this.FIELD_EP);
    }

    public createField(data: UserField): Observable<UserField> {
        return this.http.post<UserField>(this.FIELD_EP, data);
    }

    public updateField(data: UserField): Observable<UserField> {
        return this.http.put<UserField>(this.FIELD_EP, data);
    }

    public deleteField(data: UserField): Observable<UserField> {
        return this.http.delete<UserField>(this.FIELD_EP + '/' + data.id);
    }

}
