import {Component, OnInit} from '@angular/core';
import {User, UserField} from "../user";
import {UserService} from "../user.service";
import {UserListComponent} from "../user-list/user-list.component";
import {FieldService} from "../field.service";
import {NotificationService} from "../notification.service";

@Component({
    selector: 'app-user-creation-form',
    templateUrl: './user-creation-form.component.html',
    styles: []
})
export class UserCreationFormComponent implements OnInit {

    private user: User;
    private customFields: Array<UserField>;

    constructor(private userService: UserService,
                private fieldService: FieldService,
                private notificationService: NotificationService,
                private userListComponent: UserListComponent) {
    }

    ngOnInit(): void {
        this.refreshFields();
        this.fieldService.getAllFields().subscribe(fields => {
            this.customFields = fields;
            this.customFields.forEach(field => this.user.customFields.push({
                value: field.defaultValue,
                type: field
            }))
        })
    }

    private createUser(): void {
        this.userService.createUser(this.user)
            .subscribe(createdUser => {
                this.notificationService.showSuccess('User ' + createdUser.username + ' created.');
                this.refreshFields();
                this.userListComponent.addDisplayUser(createdUser)
            })
    }

    private refreshFields(): void {
        this.user = {
            password: '',
            username: '',
            enabled: true,
            customFields: []
        }
    }

}
