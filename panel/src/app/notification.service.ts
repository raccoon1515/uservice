import {Injectable} from '@angular/core';
import {MatSnackBar} from "@angular/material/snack-bar";

@Injectable({
    providedIn: 'root'
})
export class NotificationService {

    constructor(public snackBar: MatSnackBar) {
    }

    public showSuccess(message: string): void {
        this.snackBar.open(message, 'X', {duration: 3000});
    }

    public showError(message: string): void {
        this.snackBar.open(message, 'X', {panelClass: ['error']});
    }

}
