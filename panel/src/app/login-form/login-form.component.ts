import {Component} from '@angular/core';
import {AuthService} from "../auth.service";
import {MatDialogRef} from "@angular/material/dialog";

@Component({
  selector: 'app-login-form',
  templateUrl: './login-form.component.html',
  styleUrls: ['./login-form.component.scss']
})
export class LoginFormComponent {

    private credentials = {
        username: '',
        password: ''
    };

  constructor(public dialogRef: MatDialogRef<LoginFormComponent>,
              private authService: AuthService) { }

  login(): void {
      this.authService.login(this.credentials);
      this.dialogRef.close();
  }

}
