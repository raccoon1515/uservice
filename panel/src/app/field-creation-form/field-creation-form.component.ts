import {Component, OnInit} from '@angular/core';
import {FieldService} from "../field.service";
import {NotificationService} from "../notification.service";
import {UserField} from "../user";

@Component({
    selector: 'app-field-creation-form',
    templateUrl: './field-creation-form.component.html',
    styleUrls: ['./field-creation-form.component.scss']
})
export class FieldCreationFormComponent implements OnInit {
    private field: UserField = {
        name: '',
        type: '',
        defaultValue: ''
    };
    private fieldTypes: string[];

    constructor(private fieldService: FieldService,
                private notificationService: NotificationService) {
    }

    ngOnInit(): void {
        this.fieldService.getAllFieldTypes().subscribe(fieldTypes => this.fieldTypes = fieldTypes)
    }

    private createField(): void {
        this.fieldService.createField(this.field)
            .subscribe(createdField =>
                this.notificationService.showSuccess('Field ' + createdField.name + ' created.'));
    }

}
