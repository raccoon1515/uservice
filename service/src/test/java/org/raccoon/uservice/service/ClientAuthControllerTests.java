package org.raccoon.uservice.service;

import org.json.JSONObject;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.raccoon.uservice.service.api.http.UserController;
import org.raccoon.uservice.service.config.ApplicationConfig;
import org.raccoon.uservice.service.config.WebSecurityConfig;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.web.servlet.MockMvc;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

// fixme: test not running

@RunWith(SpringJUnit4ClassRunner.class)
@WebMvcTest(UserController.class)
@ContextConfiguration(classes = {ApplicationConfig.class, WebSecurityConfig.class})
public class ClientAuthControllerTests {

    @Autowired
    private  MockMvc mockMvc;

     @Test
    public void clientAuthTest() throws Exception {
         final JSONObject body = new JSONObject();
         body.put("username", "user");
         body.put("password", "user");

         mockMvc.perform(get("").contentType(MediaType.APPLICATION_JSON))
                 .andExpect(status().isOk());
     }

}
