package org.raccoon.uservice.service.api.http.auth;

import org.raccoon.uservice.service.converter.BearerToken;
import org.raccoon.uservice.service.dto.AuthRequest;
import org.raccoon.uservice.service.dto.MessageResponse;
import org.raccoon.uservice.service.dto.TokenPair;
import org.raccoon.uservice.service.service.ServiceAuthService;
import org.springframework.web.bind.annotation.*;

import static org.raccoon.uservice.service.consts.MvcEndpoints.SERVICE_AUTH;
import static org.raccoon.uservice.service.consts.MvcEndpoints.SERVICE_REFRESH;
import static org.raccoon.uservice.service.consts.MvcEndpoints.SERVICE_SIGNIN;
import static org.raccoon.uservice.service.consts.MvcEndpoints.SERVICE_SIGNOUT;
import static org.raccoon.uservice.service.consts.MvcEndpoints.SERVICE_VALIDATE;

/**
 * {@link org.raccoon.uservice.service.entity.service.ServiceUser}
 * authentication controller implementation.
 */
@RestController
@RequestMapping(SERVICE_AUTH)
public class ServiceAuthController implements AuthController {

    private final ServiceAuthService serviceAuthService;

    public ServiceAuthController(ServiceAuthService serviceAuthService) {
        this.serviceAuthService = serviceAuthService;
    }

    @Override
    @PostMapping(SERVICE_SIGNIN)
    public TokenPair signIn(@RequestBody AuthRequest request) {
        return serviceAuthService.authenticate(request);
    }

    @Override
    @PostMapping(SERVICE_REFRESH)
    public TokenPair refresh(@BearerToken String refreshToken) {
        return serviceAuthService.refreshTokens(refreshToken);
    }

    @Override
    @PostMapping(SERVICE_SIGNOUT)
    public MessageResponse signOut(@BearerToken String accessToken) {
        serviceAuthService.deAuthenticate(accessToken);
        return new MessageResponse("Logged out");
    }

    @Override
    @PostMapping(SERVICE_VALIDATE)
    public MessageResponse validate(@BearerToken String accessToken) {
        return new MessageResponse("Token valid");
    }

}
