package org.raccoon.uservice.service.dto;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;

/**
 * Response with a message.
 */
@Getter
@Setter
@RequiredArgsConstructor
public class MessageResponse {

    public MessageResponse(String message) {
        this.message = message;
        this.success = true;
    }

    private final String message;
    private final boolean success;
}
