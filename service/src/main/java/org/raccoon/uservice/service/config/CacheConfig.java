package org.raccoon.uservice.service.config;

import com.github.benmanes.caffeine.cache.Caffeine;
import org.jetbrains.annotations.Contract;
import org.jetbrains.annotations.NotNull;
import org.raccoon.uservice.service.consts.CacheNames;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cache.CacheManager;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.cache.caffeine.CaffeineCache;
import org.springframework.cache.support.SimpleCacheManager;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.*;
import java.util.concurrent.TimeUnit;

/**
 * Cache configuration.
 */
@Configuration
@EnableCaching
public class CacheConfig {

    @Bean
    public CacheManager cacheManager(@Value("${token.lifetime.access:15}") int accessTokenLifetime,
                                     @Value("${cache.users.ttl:50}") int usersCacheTtl) {
        final SimpleCacheManager manager = new SimpleCacheManager();

        final List<CaffeineCache> caches = Arrays.asList(
                buildCache(CacheNames.INVALID_TOKENS, accessTokenLifetime, 1000),
                buildCache(CacheNames.CLIENT_USERS, usersCacheTtl, 1000));

        manager.setCaches(caches);
        return manager;
    }

    @NotNull
    @Contract("_, _, _ -> new")
    @SuppressWarnings("SameParameterValue")
    private CaffeineCache buildCache(@NotNull String name, int minutesToExpire, int maxSize) {
        return new CaffeineCache(name, Caffeine.newBuilder()
                .expireAfterWrite(minutesToExpire, TimeUnit.MINUTES)
                .maximumSize(maxSize)
                .build());
    }

}
