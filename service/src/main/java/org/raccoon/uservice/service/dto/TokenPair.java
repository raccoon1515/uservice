package org.raccoon.uservice.service.dto;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

/**
 * Pair of access & refresh JWT.
 */
@Getter
@RequiredArgsConstructor
public class TokenPair {
    private final String accessToken;
    private final String refreshToken;

}
