package org.raccoon.uservice.service.security;

import io.jsonwebtoken.ExpiredJwtException;
import io.jsonwebtoken.JwtException;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.jetbrains.annotations.NotNull;
import org.raccoon.uservice.service.consts.MvcEndpoints;
import org.raccoon.uservice.service.dto.ErrorResponse;
import org.raccoon.uservice.service.utils.JsonUtils;
import org.raccoon.uservice.service.utils.ServletUtils;
import org.raccoon.uservice.service.utils.TokenUtils;
import org.springframework.cache.Cache;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.filter.GenericFilterBean;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import java.io.IOException;

@Slf4j
@RequiredArgsConstructor
public class JwtFilter extends GenericFilterBean {

    private final JwtProvider jwtProvider;
    private final Cache invalidJwt;

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
            throws IOException, ServletException {

        final HttpServletRequest httpServletRequest = (HttpServletRequest) request;
        String token = TokenUtils.getBearerToken(httpServletRequest);
        if (token != null) {
            try {
                token = TokenUtils
                        .getNativeToken(httpServletRequest)
                        .orElseThrow(() -> new JwtException("Invalid JWT"));

                jwtProvider.validateToken(token);

                final String invalidUserName = invalidJwt.get(token, String.class);
                if (invalidUserName != null) {
                    throw new JwtException("Token marked as invalid");
                }
                log.debug("Received valid token: {}", token);

                final Authentication auth = httpServletRequest.getRequestURI().startsWith(MvcEndpoints.SERVICE)
                        ? jwtProvider.getServiceAuthentication(token)
                        : jwtProvider.getClientAuthentication(token);

                SecurityContextHolder.getContext().setAuthentication(auth);
            } catch (JwtException | IllegalArgumentException e) {
                log.error("Invalid token: {}", token);
                sendError(response, e);
                return;
            }
        }
        chain.doFilter(request, response);
    }

    private void sendError(@NotNull ServletResponse rsp, @NotNull Exception exception) throws IOException {
        final HttpServletResponse response = (HttpServletResponse) rsp;

        // Do not send a large message of ExpiredJwtException
        final String message = exception instanceof ExpiredJwtException
                ? "JWT expired"
                : exception.getMessage();
        final String responseBody = JsonUtils.convertToJson(new ErrorResponse(message));
        ServletUtils.sendJsonMessage(response, HttpStatus.UNAUTHORIZED, responseBody);
    }

}
