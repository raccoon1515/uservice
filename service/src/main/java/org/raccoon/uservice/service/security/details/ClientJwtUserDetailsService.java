package org.raccoon.uservice.service.security.details;


import org.raccoon.uservice.service.entity.client.ClientUser;
import org.raccoon.uservice.service.repository.client.ClientUserRepository;
import org.raccoon.uservice.service.security.JwtUserDetailsFactory;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

@Service
public class ClientJwtUserDetailsService implements UserDetailsService {

    private final ClientUserRepository clientUserRepository;

    public ClientJwtUserDetailsService(ClientUserRepository clientUserRepository) {
        this.clientUserRepository = clientUserRepository;
    }

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        final ClientUser clientUser = clientUserRepository.findUserByUsername(username);
        if (clientUser == null) {
            throw new UsernameNotFoundException("Username " + username + " not found");
        }
        return JwtUserDetailsFactory.createFrom(clientUser);
    }

}
