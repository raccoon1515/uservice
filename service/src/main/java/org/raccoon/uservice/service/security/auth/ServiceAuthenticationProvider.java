package org.raccoon.uservice.service.security.auth;

import org.raccoon.uservice.service.entity.service.ServiceUser;
import org.raccoon.uservice.service.repository.UserRepository;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;

import java.util.*;

@Component
public class ServiceAuthenticationProvider extends AbstractAuthenticationProvider<ServiceUser> {

    public ServiceAuthenticationProvider(UserRepository<ServiceUser> userRepository,
                                         PasswordEncoder passwordEncoder) {
        super(userRepository, passwordEncoder);
    }

    @Override
    public boolean supports(Class<?> authentication) {
        return authentication.equals(ServiceAuthenticationToken.class);
    }

    public static class ServiceAuthenticationToken extends UsernamePasswordAuthenticationToken {

        public ServiceAuthenticationToken(Object principal, Object credentials) {
            super(principal, credentials);
        }

        public ServiceAuthenticationToken(Object principal, Object credentials,
                                          Collection<? extends GrantedAuthority> authorities) {
            super(principal, credentials, authorities);
        }
    }

}
