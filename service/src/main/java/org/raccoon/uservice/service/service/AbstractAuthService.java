package org.raccoon.uservice.service.service;

import org.jetbrains.annotations.NotNull;
import org.raccoon.uservice.service.consts.CacheNames;
import org.raccoon.uservice.service.dto.AuthRequest;
import org.raccoon.uservice.service.dto.TokenPair;
import org.raccoon.uservice.service.entity.Jwt;
import org.raccoon.uservice.service.entity.User;
import org.raccoon.uservice.service.exception.TokenNotFoundException;
import org.raccoon.uservice.service.repository.JwtRepository;
import org.raccoon.uservice.service.repository.UserRepository;
import org.raccoon.uservice.service.security.JwtProvider;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.cache.Cache;
import org.springframework.cache.CacheManager;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.AuthenticationException;

import java.util.Collections;
import java.util.Map;

/**
 * Base class for client/service authentication services.
 *
 * @param <J> JWT type
 * @param <U> User type.
 */
public abstract class AbstractAuthService<J extends Jwt, U extends User> {

    protected final UserRepository<U> userRepository;

    private final JwtProvider jwtProvider;
    private final JwtRepository<J> jwtRepository;
    private final AuthenticationManager authenticationManager;
    private final Cache invalidJwts;


    public AbstractAuthService(JwtProvider jwtProvider,
                               JwtRepository<J> jwtRepository,
                               CacheManager cacheManager,
                               @Qualifier("authenticationManagerBean") AuthenticationManager authenticationManager,
                               UserRepository<U> userRepository) {
        this.jwtProvider = jwtProvider;
        this.jwtRepository = jwtRepository;
        this.invalidJwts = cacheManager.getCache(CacheNames.INVALID_TOKENS);
        this.authenticationManager = authenticationManager;
        this.userRepository = userRepository;
    }

    /**
     * Tries to authenticate user.
     *
     * @return pair of access & refresh JWTs
     * @throws AuthenticationException if authentication fails.
     */
    @NotNull
    public TokenPair authenticate(@NotNull AuthRequest request) throws AuthenticationException {
        final String username = request.getUsername();
        final String password = request.getPassword();
        Map<String, Object> requestMetadata = request.getMetadata();
        if (requestMetadata == null) {
            requestMetadata = Collections.emptyMap();
        }

        authenticationManager.authenticate(createAuthToken(username, password));
        final U user = userRepository.findUserByUsername(username);

        J jwt = jwtRepository.findJwtTokenByUserUsername(username);
        if (jwt == null) {
            jwt = createJwt(user);
        }

        final Map<String, Object> savedMetadata = getJwtMetadata(user);
        requestMetadata.putAll(savedMetadata);

        final TokenPair tokenPair = jwtProvider.createTokenPair(username, requestMetadata);

        jwt.setToken(tokenPair.getRefreshToken());
        jwtRepository.save(jwt);
        return tokenPair;
    }

    /**
     * Refreshes user token.
     *
     * @param refreshToken a user refresh JWT
     * @return new pair of access & refresh JWTs.
     */
    @NotNull
    public TokenPair refreshTokens(@NotNull String refreshToken) {
        final String username = jwtProvider.getUsername(refreshToken);

        final TokenPair tokenPair = jwtProvider.createTokenPair(username);

        final J jwt = jwtRepository.findJwtTokenByToken(refreshToken);
        if (jwt == null) {
            throw new TokenNotFoundException("Token cannot be refreshed, cause absent in db");
        }

        jwt.setToken(tokenPair.getRefreshToken());
        jwtRepository.save(jwt);

        // todo markInvalid();

        return tokenPair;
    }

    /**
     * De authenticates user.
     *
     * @param accessToken the user access JWT.
     */
    public void deAuthenticate(@NotNull String accessToken) {
        final String username = jwtProvider.getUsername(accessToken);
        jwtRepository.deleteJwtByUserUsername(username);
        markInvalid(accessToken, username);
    }

    /**
     * Marks access JWT as invalid.
     * This JWT will be stored in a cache.
     *
     * @param accessToken the access JWT that will be marked as invalid
     * @param username a user name.
     */
    private void markInvalid(@NotNull String accessToken, @NotNull String username) {
        invalidJwts.put(accessToken, username);
    }

    /**
     * Creates a native JWT object.
     *
     * @param user the user to be assigned JWT
     * @return created JWT
     */
    protected abstract J createJwt(@NotNull U user);

    /**
     * Creates an auth-service unique security authentication token.
     *
     * @param username a user name
     * @param password a user raw password
     * @return security authentication token.
     */
    protected abstract UsernamePasswordAuthenticationToken createAuthToken(
            String username,
            String password);

    /**
     * Gets an additional stored metadata from a db.
     * This metadata will be added to token payload.
     *
     * @param user the User, which metadata will be taken
     * @return user metadata.
     */
    protected Map<String, Object> getJwtMetadata(@NotNull U user) {
        return Collections.emptyMap();
    }

}
