package org.raccoon.uservice.service.dto;

/**
 * Error response object
 */
public class ErrorResponse extends MessageResponse {

    public ErrorResponse(String message) {
        super(message, false);
    }

}
