package org.raccoon.uservice.service.exception;

/**
 * Occurs when any resource doesn't exist.
 */
public class ResourceNotFoundException extends UserviceException {

    public ResourceNotFoundException(String message, Throwable cause) {
        super(message, cause);
    }

    public ResourceNotFoundException(String message) {
        super(message);
    }

}
