package org.raccoon.uservice.service.entity;

import lombok.Data;
import org.hibernate.annotations.Type;

import javax.persistence.*;

/**
 * Base class for JWT entities.
 */
@Data
@MappedSuperclass
public abstract class Jwt {

    /**
     * Jwt database id.
     */
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    private long id;

    /**
     * Refresh JWT value.
     */
    @Type(type = "text")
    @Column(name = "token", nullable = false)
    private String token;

}
