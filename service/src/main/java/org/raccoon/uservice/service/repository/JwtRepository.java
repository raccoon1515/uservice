package org.raccoon.uservice.service.repository;

import org.raccoon.uservice.service.entity.Jwt;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.NoRepositoryBean;

/**
 * Base JWT repository.
 *
 * @param <T> JWT type.
 * @see org.raccoon.uservice.service.repository.client.ClientJwtRepository
 * @see org.raccoon.uservice.service.repository.service.ServiceJwtRepository
 */
@NoRepositoryBean
public interface JwtRepository<T extends Jwt> extends CrudRepository<T, Long> {

    T findJwtTokenByUserUsername(String username);

    void deleteJwtByUserUsername(String username);

    T findJwtTokenByToken(String refreshToken);
}
