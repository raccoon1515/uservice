package org.raccoon.uservice.service.security.auth;

import org.raccoon.uservice.service.entity.client.ClientUser;
import org.raccoon.uservice.service.repository.UserRepository;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;

@Component
public class ClientAuthenticationProvider extends AbstractAuthenticationProvider<ClientUser> {

    public ClientAuthenticationProvider(UserRepository<ClientUser> userRepository,
                                        PasswordEncoder passwordEncoder) {
        super(userRepository, passwordEncoder);
    }

    @Override
    public boolean supports(Class<?> authentication) {
        return authentication.equals(ClientAuthenticationToken.class);
    }

    public static class ClientAuthenticationToken extends UsernamePasswordAuthenticationToken {

        public ClientAuthenticationToken(Object principal, Object credentials) {
            super(principal, credentials);
        }

    }

}
