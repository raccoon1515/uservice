package org.raccoon.uservice.service.security;

import org.raccoon.uservice.service.dto.ErrorResponse;
import org.raccoon.uservice.service.utils.JsonUtils;
import org.raccoon.uservice.service.utils.ServletUtils;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.AccessDeniedException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import java.io.IOException;

public class AccessDeniedHandler implements org.springframework.security.web.access.AccessDeniedHandler {

    @Override
    public void handle(HttpServletRequest request, HttpServletResponse response,
                       AccessDeniedException accessDeniedException) throws IOException, ServletException {
        final ErrorResponse message = new ErrorResponse("Access denied for this user");
        final String responseBody = JsonUtils.convertToJson(message);
        ServletUtils.sendJsonMessage(response, HttpStatus.FORBIDDEN, responseBody);
    }

}
