package org.raccoon.uservice.service.repository.client;

import org.raccoon.uservice.service.entity.client.ClientJwt;
import org.raccoon.uservice.service.repository.JwtRepository;
import org.springframework.stereotype.Repository;

/**
 * Client JWT repository.
 */
@Repository
public interface ClientJwtRepository extends JwtRepository<ClientJwt> {

    void deleteClientJwtByUserId(long userId);
}
