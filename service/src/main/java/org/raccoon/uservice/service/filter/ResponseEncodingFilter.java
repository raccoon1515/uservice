package org.raccoon.uservice.service.filter;

import org.springframework.stereotype.Component;

import javax.servlet.*;

import java.io.IOException;
import java.nio.charset.StandardCharsets;

/**
 * Sets UTF-8 response encoding.
 */
@Component
public class ResponseEncodingFilter implements Filter {

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {
    }

    @Override
    public void doFilter(ServletRequest request, ServletResponse response,
                         FilterChain chain) throws IOException, ServletException {
        response.setCharacterEncoding(StandardCharsets.UTF_8.name());
        chain.doFilter(request, response);
    }

    @Override
    public void destroy() {
    }

}
