package org.raccoon.uservice.service.converter;

import java.lang.annotation.*;

/**
 * Marker for resolving JWT from request header.
 *
 * @see HeaderTokenArgumentResolver
 */
@Documented
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.PARAMETER)
public @interface BearerToken {
}
