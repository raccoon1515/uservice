package org.raccoon.uservice.service;

import org.springframework.boot.Banner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.web.servlet.error.ErrorMvcAutoConfiguration;
import org.springframework.context.ConfigurableApplicationContext;

import java.util.*;

/**
 * Main Spring Boot UService class.
 */
@SpringBootApplication(exclude = ErrorMvcAutoConfiguration.class)
public class ServiceApplication {

    public static void main(String[] args) {
        final SpringApplication application = new SpringApplication(ServiceApplication.class);
        final Properties properties = new Properties();
        properties.put("spring.resources.add-mappings", false);
        properties.put("spring.mvc.throw-exception-if-no-handler-found", true);
        properties.put("server.error.whitelabel.enabled", false);
        properties.put("spring.jpa.show-sql", true);
        properties.put("spring.jpa.hibernate.ddl-auto", "validate");

        application.setBannerMode(Banner.Mode.OFF);
        application.setDefaultProperties(properties);
        application.run(args);
    }

}
