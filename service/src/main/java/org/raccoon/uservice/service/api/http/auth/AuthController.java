package org.raccoon.uservice.service.api.http.auth;

import org.raccoon.uservice.service.converter.BearerToken;
import org.raccoon.uservice.service.dto.AuthRequest;
import org.raccoon.uservice.service.dto.MessageResponse;
import org.raccoon.uservice.service.dto.TokenPair;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 * Authentication controller base.
 */
public interface AuthController {

    /**
     * Handles authentication request.
     * Creates a new pair of {@link TokenPair}.
     */
    @ResponseStatus(HttpStatus.OK)
    TokenPair signIn(@RequestBody AuthRequest request);

    /**
     * Handles refresh token request.
     * Deletes an old refresh JWT and
     * creates a new {@link TokenPair}.
     */
    @ResponseStatus(HttpStatus.OK)
    TokenPair refresh(@BearerToken String refreshToken);

    /**
     * Handles logout request.
     * Deletes a refresh JWT from a database.
     */
    @ResponseStatus(HttpStatus.OK)
    MessageResponse signOut(@BearerToken String accessToken);

    /**
     * Handles validate access JWT request.
     * Simply returns success message, cause
     * otherwise user will not reach here.
     */
    @ResponseStatus(HttpStatus.OK)
    MessageResponse validate(@BearerToken String accessToken);

}
