package org.raccoon.uservice.service.consts;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;

/**
 * Application controllers mappings.
 */
@NoArgsConstructor(access = AccessLevel.PRIVATE)
public final class MvcEndpoints {

    /**
     * Common endpoint for service actions.
     * Service authentication and user creation endpoints.
     */
    public static final String SERVICE = "/service";

    // Service controller
    public static final String SERVICE_AUTH = SERVICE + "/auth";
    public static final String SERVICE_SIGNIN = "/signin";
    public static final String SERVICE_REFRESH = "/refresh";
    public static final String SERVICE_SIGNOUT = "/signout";
    public static final String SERVICE_VALIDATE = "/valid";

    // User controller
    public static final String SERVICE_USER = SERVICE + "/user";

    // Client controller
    public static final String CLIENT_AUTH = "/";
    public static final String CLIENT_SIGNIN = "/signin";
    public static final String CLIENT_REFRESH = "/refresh";
    public static final String CLIENT_SIGNOUT = "/signout";
    public static final String CLIENT_VALIDATE = "/valid";

    // Public key controller
    public static final String PUBLIC_KEY = "/key";

    @NotNull
    public static String asPattern(String path) {
        if (path == null) {
            throw new IllegalArgumentException("Endpoint path cannot be null");
        }
        return path + "/**";
    }

}
