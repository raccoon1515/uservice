package org.raccoon.uservice.service.api.http;

import org.raccoon.uservice.service.dto.MessageResponse;
import org.raccoon.uservice.service.entity.client.ClientUser;
import org.raccoon.uservice.service.service.UserService;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.security.Principal;

import static org.raccoon.uservice.service.consts.MvcEndpoints.SERVICE_USER;

@RestController
@RequestMapping(SERVICE_USER)
public class UserController {

    private final UserService userService;

    public UserController(UserService userService) {
        this.userService = userService;
    }

    @GetMapping("/{userId}")
    @ResponseStatus(HttpStatus.OK)
    public ClientUser getUser(@PathVariable long userId, Principal principal) {
        final String serviceName = principal.getName();
        return userService.getUserById(userId, serviceName);
    }

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public ClientUser createUser(@RequestBody ClientUser user, Principal principal) {
        final String serviceName = principal.getName();
        return userService.createUser(user, serviceName);
    }

    @PutMapping
    @ResponseStatus(HttpStatus.OK)
    public ClientUser updateUser(@RequestBody ClientUser user, Principal principal) {
        final String serviceName = principal.getName();
        return userService.updateUser(user, serviceName);
    }

    @DeleteMapping("/{userId}")
    @ResponseStatus(HttpStatus.OK)
    public MessageResponse deleteUser(@PathVariable long userId, Principal principal) {
        final String serviceName = principal.getName();
        userService.deleteUser(userId, serviceName);
        return new MessageResponse("User successfully deleted");
    }

}
