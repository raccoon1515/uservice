package org.raccoon.uservice.service.repository.client;

import org.raccoon.uservice.service.entity.client.ClientUser;
import org.raccoon.uservice.service.repository.UserRepository;
import org.springframework.stereotype.Repository;

import java.util.*;

/**
 * Client user repository.
 */
@Repository
public interface ClientUserRepository extends UserRepository<ClientUser> {

    Optional<ClientUser> findClientUserByIdAndServiceId(long userId, long serviceId);
}
