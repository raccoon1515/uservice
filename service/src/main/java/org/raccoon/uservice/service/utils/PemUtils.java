package org.raccoon.uservice.service.utils;

import lombok.experimental.UtilityClass;
import lombok.extern.slf4j.Slf4j;
import org.bouncycastle.util.io.pem.PemObject;
import org.bouncycastle.util.io.pem.PemReader;
import org.jetbrains.annotations.NotNull;

import java.io.*;
import java.nio.file.Files;
import java.security.*;
import java.security.spec.*;

import static java.nio.charset.StandardCharsets.UTF_8;

/**
 * Reads and pareses PEM keys.
 */
@Slf4j
@UtilityClass
public final class PemUtils {

    public static PublicKey readPublicKeyFromFile(final String filepath, final String algorithm) throws IOException {
        final byte[] bytes = PemUtils.parsePEMFile(new File(filepath));
        return PemUtils.getPublicKey(bytes, algorithm);
    }

    public static PrivateKey readPrivateKeyFromFile(final String filepath, final String algorithm) throws IOException {
        final byte[] bytes = PemUtils.parsePEMFile(new File(filepath));
        return PemUtils.getPrivateKey(bytes, algorithm);
    }

    private static PublicKey getPublicKey(final byte[] keyBytes, final String algorithm) {
        try {
            final KeyFactory kf = KeyFactory.getInstance(algorithm);
            final EncodedKeySpec keySpec = new X509EncodedKeySpec(keyBytes);
            return kf.generatePublic(keySpec);
        } catch (NoSuchAlgorithmException e) {
            log.error("Could not reconstruct the public key, the given algorithm could not be found.");
            throw new RuntimeException(e);
        } catch (InvalidKeySpecException e) {
            log.error("Could not reconstruct the public key");
            throw new RuntimeException(e);
        }
    }

    private static PrivateKey getPrivateKey(final byte[] keyBytes, final String algorithm) {
        try {
            final KeyFactory kf = KeyFactory.getInstance(algorithm);
            final EncodedKeySpec keySpec = new PKCS8EncodedKeySpec(keyBytes);
            return kf.generatePrivate(keySpec);
        } catch (NoSuchAlgorithmException e) {
            log.error("Could not reconstruct the private key, the given algorithm could not be found.");
            throw new RuntimeException(e);
        } catch (InvalidKeySpecException e) {
            log.error("Could not reconstruct the private key");
            throw new RuntimeException(e);
        }
    }

    private static byte[] parsePEMFile(@NotNull final File pemFile) throws IOException {
        if (!pemFile.isFile() || !pemFile.exists()) {
            throw new FileNotFoundException(String.format("The file '%s' doesn't exist.", pemFile.getAbsolutePath()));
        }
        try (PemReader reader = new PemReader(Files.newBufferedReader(pemFile.toPath(), UTF_8))) {
            final PemObject pemObject = reader.readPemObject();
            return pemObject.getContent();
        }
    }

}
