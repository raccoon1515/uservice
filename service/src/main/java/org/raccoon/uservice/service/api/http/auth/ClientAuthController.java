package org.raccoon.uservice.service.api.http.auth;

import org.raccoon.uservice.service.converter.BearerToken;
import org.raccoon.uservice.service.dto.AuthRequest;
import org.raccoon.uservice.service.dto.MessageResponse;
import org.raccoon.uservice.service.dto.TokenPair;
import org.raccoon.uservice.service.service.ClientAuthService;
import org.springframework.web.bind.annotation.*;

import static org.raccoon.uservice.service.consts.MvcEndpoints.CLIENT_AUTH;
import static org.raccoon.uservice.service.consts.MvcEndpoints.CLIENT_REFRESH;
import static org.raccoon.uservice.service.consts.MvcEndpoints.CLIENT_SIGNIN;
import static org.raccoon.uservice.service.consts.MvcEndpoints.CLIENT_SIGNOUT;
import static org.raccoon.uservice.service.consts.MvcEndpoints.CLIENT_VALIDATE;
import static org.raccoon.uservice.service.consts.MvcEndpoints.SERVICE_VALIDATE;

/**
 * {@link org.raccoon.uservice.service.entity.client.ClientUser}
 * authentication controller implementation.
 */
@RestController
@RequestMapping(CLIENT_AUTH)
public class ClientAuthController implements AuthController {

    private final ClientAuthService authService;

    public ClientAuthController(ClientAuthService authService) {
        this.authService = authService;
    }

    @Override
    @PostMapping(CLIENT_SIGNIN)
    public TokenPair signIn(@RequestBody AuthRequest request) {
        return authService.authenticate(request);
    }

    @Override
    @PostMapping(CLIENT_REFRESH)
    public TokenPair refresh(@BearerToken String refreshToken) {
        return authService.refreshTokens(refreshToken);
    }

    @Override
    @PostMapping(CLIENT_SIGNOUT)
    public MessageResponse signOut(@BearerToken String accessToken) {
        authService.deAuthenticate(accessToken);
        return new MessageResponse("Logged out");
    }

    @Override
    @PostMapping(CLIENT_VALIDATE)
    public MessageResponse validate(@BearerToken String accessToken) {
        return new MessageResponse("Token valid");
    }

}
