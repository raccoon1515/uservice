package org.raccoon.uservice.service.exception;

import org.springframework.security.core.AuthenticationException;

/**
 * Occurs when refresh token not present in a database.
 */
public class TokenNotFoundException extends AuthenticationException {

    public TokenNotFoundException(String msg, Throwable t) {
        super(msg, t);
    }

    public TokenNotFoundException(String msg) {
        super(msg);
    }
}
