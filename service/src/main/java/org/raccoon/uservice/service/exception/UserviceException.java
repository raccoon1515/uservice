package org.raccoon.uservice.service.exception;

/**
 * Any custom exception.
 */
public class UserviceException extends RuntimeException {

    public UserviceException(String message) {
        super(message);
    }

    public UserviceException(String message, Throwable cause) {
        super(message, cause);
    }
}
