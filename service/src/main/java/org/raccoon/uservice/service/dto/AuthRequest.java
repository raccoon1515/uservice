package org.raccoon.uservice.service.dto;

import lombok.*;

import java.util.Map;

/**
 * Authentication request dto.
 * Must contain {@code username} and {@code password} fields.
 * {@code metadata} is an optional field.
 */
@Getter
@Setter
@RequiredArgsConstructor
public class AuthRequest {

    /**
     * User name.
     * Must present in a database.
     */
    private final String username;
    /**
     * User raw password.
     */
    private final String password;
    /**
     * Optional metadata to be injected in JWT payload.
     */
    private final Map<String, Object> metadata;
}
