package org.raccoon.uservice.service.converter.proto;

import org.jetbrains.annotations.NotNull;
import org.raccoon.uservice.client.grpc.*;
import org.raccoon.uservice.service.entity.client.ClientUser;
import org.raccoon.uservice.service.entity.service.Role;

import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

/**
 * Converts {@link ClientUser} to proto.
 */
public final class UserProtoConverter {

    @NotNull
    public static UserProto convertUserToProto(@NotNull ClientUser clientUser) {
        return UserProto.newBuilder()
                .setUserId(clientUser.getId())
                .setUsername(clientUser.getUsername())
                .setPassword(clientUser.getPassword())
                .build();
    }

    @NotNull
    public static UserListProto convertUserListToProto(@NotNull Iterable<ClientUser> users) {
        return UserListProto.newBuilder()
                .addAllUsers(StreamSupport.stream(users.spliterator(), false)
                        .map(UserProtoConverter::convertUserToProto)
                        .collect(Collectors.toSet()))
                .build();
    }

    @NotNull
    public static RoleProto convertRoleToProto(@NotNull Role serviceRole) {
        return RoleProto.newBuilder()
                .setRoleId(serviceRole.getId())
                .setName(serviceRole.getName())
                .build();
    }

    @NotNull
    public static RoleListProto convertRoleListToProto(Collection<Role> roles) {
        return RoleListProto.newBuilder()
                .addAllRoles(roles.stream()
                        .map(UserProtoConverter::convertRoleToProto)
                        .collect(Collectors.toSet()))
                .build();
    }

}
