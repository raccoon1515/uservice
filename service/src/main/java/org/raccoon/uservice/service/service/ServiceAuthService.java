package org.raccoon.uservice.service.service;

import org.jetbrains.annotations.NotNull;
import org.raccoon.uservice.service.entity.service.ServiceJwt;
import org.raccoon.uservice.service.entity.service.ServiceUser;
import org.raccoon.uservice.service.repository.JwtRepository;
import org.raccoon.uservice.service.repository.UserRepository;
import org.raccoon.uservice.service.security.JwtProvider;
import org.raccoon.uservice.service.security.auth.ServiceAuthenticationProvider.ServiceAuthenticationToken;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.cache.CacheManager;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.stereotype.Service;

/**
 * Service authentication service.
 */
@Service
public class ServiceAuthService extends AbstractAuthService<ServiceJwt, ServiceUser> {


    public ServiceAuthService(JwtProvider jwtProvider,
                              JwtRepository<ServiceJwt> jwtRepository,
                              CacheManager cacheManager,
                              @Qualifier("authenticationManagerBean") AuthenticationManager authenticationManager,
                              UserRepository<ServiceUser> userRepository) {
        super(jwtProvider, jwtRepository, cacheManager, authenticationManager, userRepository);
    }

    @Override
    protected UsernamePasswordAuthenticationToken createAuthToken(String username, String password) {
        return new ServiceAuthenticationToken(username, password);
    }

    @Override
    protected ServiceJwt createJwt(@NotNull ServiceUser user) {
        final ServiceJwt jwt = new ServiceJwt();
        jwt.setUser(user);
        return jwt;
    }

}
