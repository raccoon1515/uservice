package org.raccoon.uservice.service.security.auth;

import org.raccoon.uservice.service.entity.User;
import org.raccoon.uservice.service.repository.UserRepository;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;

public abstract class AbstractAuthenticationProvider<U extends User> implements AuthenticationProvider {

    protected final UserRepository<U> userRepository;
    protected final PasswordEncoder passwordEncoder;

    protected AbstractAuthenticationProvider(UserRepository<U> userRepository,
                                             PasswordEncoder passwordEncoder) {
        this.userRepository = userRepository;
        this.passwordEncoder = passwordEncoder;
    }

    @Override
    public Authentication authenticate(Authentication authentication) throws AuthenticationException {
        final String username = authentication.getName();
        final String password = authentication.getCredentials().toString();

        final User user = userRepository.findUserByUsername(username);
        if (user == null) {
            throw new UsernameNotFoundException("Username " + username + " not found");
        }
        // fixme
        if (authentication.getPrincipal() instanceof String
                && !passwordEncoder.matches(password, user.getPassword())) {
            throw new BadCredentialsException("Password mismatch for user " + username);
        }
        if (!user.isEnabled()) {
            throw new AccessDeniedException("Access denied for user " + username);
        }

        return new UsernamePasswordAuthenticationToken(username, password);
    }

}
