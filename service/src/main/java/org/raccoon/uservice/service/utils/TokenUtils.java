package org.raccoon.uservice.service.utils;

import lombok.experimental.UtilityClass;
import org.apache.commons.lang3.StringUtils;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.http.HttpHeaders;

import javax.servlet.http.HttpServletRequest;

import java.util.*;

@UtilityClass
public final class TokenUtils {

    /**
     * Extracts bearer token from request header.
     *
     * @return Bearer token from request header or {@code null}.
     */
    @Nullable
    public static String getBearerToken(@NotNull HttpServletRequest request) {
        final String jwt = request.getHeader(HttpHeaders.AUTHORIZATION);
        return StringUtils.defaultIfBlank(jwt, null);
    }

    /**
     * Extracts bearer token from request header and replaces "Bearer " prefix.
     *
     * @return optional of bearer token from request header.
     */
    @NotNull
    public static Optional<String> getNativeToken(@NotNull HttpServletRequest request) {
        final String token = getBearerToken(request);
        if (token != null && token.startsWith("Bearer ")) {
            // remove "Bearer " prefix
            return Optional.of(token.substring(7));
        }
        return Optional.empty();
    }

}
