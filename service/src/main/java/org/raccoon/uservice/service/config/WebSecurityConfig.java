package org.raccoon.uservice.service.config;

import org.raccoon.uservice.service.consts.CacheNames;
import org.raccoon.uservice.service.security.*;
import org.raccoon.uservice.service.security.auth.ClientAuthenticationProvider;
import org.raccoon.uservice.service.security.auth.ServiceAuthenticationProvider;
import org.springframework.cache.Cache;
import org.springframework.cache.CacheManager;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpStatus;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.web.authentication.HttpStatusEntryPoint;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

import static org.raccoon.uservice.service.consts.MvcEndpoints.CLIENT_SIGNIN;
import static org.raccoon.uservice.service.consts.MvcEndpoints.PUBLIC_KEY;
import static org.raccoon.uservice.service.consts.MvcEndpoints.SERVICE;
import static org.raccoon.uservice.service.consts.MvcEndpoints.SERVICE_AUTH;
import static org.raccoon.uservice.service.consts.MvcEndpoints.SERVICE_SIGNIN;
import static org.raccoon.uservice.service.consts.MvcEndpoints.asPattern;

/**
 * Security configuration.
 */
@Configuration
@EnableGlobalMethodSecurity(prePostEnabled = true)
public class WebSecurityConfig extends WebSecurityConfigurerAdapter {

    private final JwtProvider jwtProvider;
    private final ClientAuthenticationProvider clientAuthenticationProvider;
    private final ServiceAuthenticationProvider serviceAuthenticationProvider;
    private final Cache invalidJwt;

    public WebSecurityConfig(JwtProvider jwtProvider,
                             CacheManager cacheManager,
                             ClientAuthenticationProvider clientAuthenticationProvider,
                             ServiceAuthenticationProvider serviceAuthenticationProvider) {
        this.jwtProvider = jwtProvider;
        this.invalidJwt = cacheManager.getCache(CacheNames.INVALID_TOKENS);
        this.clientAuthenticationProvider = clientAuthenticationProvider;
        this.serviceAuthenticationProvider = serviceAuthenticationProvider;
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http
                .cors()
                    .and()
                .csrf().disable()
                .sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS)
                    .and()
                .authorizeRequests()
                .antMatchers(SERVICE_AUTH + SERVICE_SIGNIN).permitAll()
                .antMatchers(CLIENT_SIGNIN).permitAll()
                .antMatchers(PUBLIC_KEY).permitAll()
                .antMatchers(asPattern(SERVICE)).hasRole("admin")
                .anyRequest().authenticated()
                    .and()
                .exceptionHandling()
                .authenticationEntryPoint(new UnauthorizedEntryPoint())
                .accessDeniedHandler(new AccessDeniedHandler())
                    .and()
                .addFilterBefore(new JwtFilter(jwtProvider, invalidJwt), UsernamePasswordAuthenticationFilter.class);
    }

    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
        auth
                .authenticationProvider(clientAuthenticationProvider)
                .authenticationProvider(serviceAuthenticationProvider);
    }

    @Override
    @Bean
    public AuthenticationManager authenticationManagerBean() throws Exception {
        return super.authenticationManagerBean();
    }

}
