package org.raccoon.uservice.service.utils;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.experimental.UtilityClass;
import org.jetbrains.annotations.NotNull;

import java.util.Map;

/**
 * Json utilities.
 */
@UtilityClass
public class JsonUtils {

    private static final ObjectMapper objectMapper = new ObjectMapper();

    /**
     * Converts object to JSON.
     *
     * @param object an object to be JSON-stingified
     * @return String-representation of JSON object
     * @throws JsonProcessingException if conversion error occurred.
     */
    public static String convertToJson(@NotNull Object object) throws JsonProcessingException {
        return objectMapper.writeValueAsString(object);
    }

    /**
     * Converts JSON to object.
     *
     * @param json string json
     * @param clazz class of object
     * @param <T> type of object
     * @return object from provided json
     * @throws JsonProcessingException if conversion error occurred.
     */
    public static <T> T readFrom(@NotNull String json, Class<T> clazz) throws JsonProcessingException {
        return objectMapper.readValue(json, clazz);
    }

}
