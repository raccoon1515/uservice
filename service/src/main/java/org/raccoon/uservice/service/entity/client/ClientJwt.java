package org.raccoon.uservice.service.entity.client;

import lombok.Data;
import lombok.EqualsAndHashCode;
import org.raccoon.uservice.service.entity.Jwt;

import javax.persistence.*;

/**
 * Client JWT database entity.
 */
@Entity
@Table(name = "client_jwt")
@Data
@EqualsAndHashCode(callSuper = true)
public class ClientJwt extends Jwt {

    /**
     * Assigned {@link ClientUser}.
     */
    @OneToOne(optional = false)
    @JoinColumn(name = "user_id")
    private ClientUser user;

}
