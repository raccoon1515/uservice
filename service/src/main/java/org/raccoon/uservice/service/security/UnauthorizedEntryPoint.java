package org.raccoon.uservice.service.security;

import org.raccoon.uservice.service.dto.ErrorResponse;
import org.raccoon.uservice.service.utils.JsonUtils;
import org.raccoon.uservice.service.utils.ServletUtils;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.AuthenticationEntryPoint;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import java.io.IOException;

public class UnauthorizedEntryPoint implements AuthenticationEntryPoint {

    @Override
    public void commence(HttpServletRequest request, HttpServletResponse response,
                         AuthenticationException authException) throws IOException, ServletException {

        final ErrorResponse message = new ErrorResponse("Invalid JWT");
        final String responseBody = JsonUtils.convertToJson(message);
        ServletUtils.sendJsonMessage(response, HttpStatus.UNAUTHORIZED, responseBody);
    }
}
