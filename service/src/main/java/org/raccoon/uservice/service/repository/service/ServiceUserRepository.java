package org.raccoon.uservice.service.repository.service;

import org.raccoon.uservice.service.entity.service.ServiceUser;
import org.raccoon.uservice.service.repository.UserRepository;
import org.springframework.stereotype.Repository;

/**
 * Service user repository.
 */
@Repository
public interface ServiceUserRepository extends UserRepository<ServiceUser> {

}
