package org.raccoon.uservice.service.entity.service;

import lombok.*;
import org.raccoon.uservice.service.entity.User;

import javax.persistence.*;
import java.util.Collection;

/**
 * Service user database entity.
 */
@Entity
@Table(name = "service_user")
@Data
@EqualsAndHashCode(callSuper = true)
public class ServiceUser extends User {

    /**
     * Assigned collection of {@link Role}.
     */
    @ManyToMany(fetch = FetchType.EAGER)
    @JoinTable(
            name = "service_users_role",
            joinColumns = @JoinColumn(name = "user_id", referencedColumnName = "id"),
            inverseJoinColumns = @JoinColumn(name = "role_id", referencedColumnName = "id"))
    private Collection<Role> roles;

}
