package org.raccoon.uservice.service.exception;

import lombok.extern.slf4j.Slf4j;
import org.jetbrains.annotations.NotNull;
import org.raccoon.uservice.service.dto.ErrorResponse;
import org.raccoon.uservice.service.utils.JsonUtils;
import org.raccoon.uservice.service.utils.ServletUtils;
import org.springframework.http.HttpStatus;
import org.springframework.http.converter.HttpMessageConversionException;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.core.AuthenticationException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.servlet.NoHandlerFoundException;

import javax.servlet.http.HttpServletResponse;

import java.io.IOException;

/**
 * Global exception handler.
 * Handles all unhandled exceptions except of
 * throwned inside spring-security authorization.
 *
 * @see org.raccoon.uservice.service.security.AccessDeniedHandler
 * @see org.raccoon.uservice.service.security.UnauthorizedEntryPoint
 */
@Slf4j
@RestControllerAdvice
public class GlobalExceptionHandlerController {

    /**
     * Handles {@link AccessDeniedException}.
     */
    @ExceptionHandler(AccessDeniedException.class)
    public void handleAccessDeniedException(HttpServletResponse res, AccessDeniedException e) throws IOException {
        log.error("Access denied: " + e.getMessage());
        sendError(res, HttpStatus.FORBIDDEN, e.getMessage());
    }

    /**
     * Handles {@link AuthenticationException}.
     */
    @ExceptionHandler(AuthenticationException.class)
    public void handleAuthenticationException(HttpServletResponse res, AuthenticationException e) throws IOException {
        log.error("Authentication failed: " + e.getMessage());
        sendError(res, HttpStatus.UNAUTHORIZED, e.getMessage());
    }

    /**
     * Handles {@link ResourceNotFoundException}
     * & {@link NoHandlerFoundException}.
     */
    @ExceptionHandler({ResourceNotFoundException.class, NoHandlerFoundException.class})
    public void handleResourceNotFoundException(HttpServletResponse res, Exception e) throws IOException {
        log.error("Resource not found: " + e);
        sendError(res, HttpStatus.NOT_FOUND, e.getMessage());
    }

    /**
     * Handles {@link UserviceException}
     * & {@link HttpMessageConversionException}.
     */
    @ExceptionHandler({UserviceException.class, HttpMessageConversionException.class})
    public void handleUserviceException(HttpServletResponse res, Exception e) throws IOException {
        log.error("Request failed: " + e);
        final String message = e instanceof HttpMessageConversionException
                ? "Bad request params"
                : e.getMessage();
        sendError(res, HttpStatus.BAD_REQUEST, message);
    }

    /**
     * Handles {@link Exception}.
     * This exception handler will be executed
     * if no match is found better.
     */
    @ExceptionHandler(Exception.class)
    public void handleException(HttpServletResponse res, Exception e) throws IOException {
        log.error("Internal error", e);
        sendError(res, HttpStatus.INTERNAL_SERVER_ERROR, "Internal server error");
    }

    /**
     * Writes error message to response as JSON.
     *
     * @param httpStatus http status
     * @param errorMessage message to be writed in response
     * @throws IOException if an input or output exception occurred.
     */
    private void sendError(@NotNull HttpServletResponse response,
                           @NotNull HttpStatus httpStatus,
                           @NotNull String errorMessage) throws IOException {
        final String message = JsonUtils.convertToJson(new ErrorResponse(errorMessage));
        ServletUtils.sendJsonMessage(response, httpStatus, message);
    }

}
