package org.raccoon.uservice.service.consts;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;

/**
 * Caches names
 */
@NoArgsConstructor(access = AccessLevel.PRIVATE)
public final class CacheNames {

    public static final String INVALID_TOKENS = "invalidTokens";
    public static final String CLIENT_USERS = "clientUsers";
    public static final String SERVICE_USERS = "serviceUsers";

}
