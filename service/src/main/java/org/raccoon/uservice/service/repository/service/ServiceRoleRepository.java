package org.raccoon.uservice.service.repository.service;

import org.raccoon.uservice.service.entity.service.Role;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

/**
 * Service user role repository.
 */
@Repository
public interface ServiceRoleRepository extends CrudRepository<Role, Long> {

}
