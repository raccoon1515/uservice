package org.raccoon.uservice.service.security;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.raccoon.uservice.service.entity.User;
import org.raccoon.uservice.service.entity.client.ClientUser;
import org.raccoon.uservice.service.entity.service.Role;
import org.springframework.security.core.authority.SimpleGrantedAuthority;

import java.util.*;
import java.util.stream.Collectors;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public final class JwtUserDetailsFactory {

    @NotNull
    public static <U extends User> JwtUserDetails createFrom(@NotNull U user,
                                                             @Nullable Collection<? extends Role> roles) {
        if (roles == null) {
            roles = Collections.emptyList();
        }
        return new JwtUserDetails(
                user.getId(),
                user.getUsername(),
                user.getPassword(),
                user.isEnabled(),
                roles.stream()
                        .map(role -> new SimpleGrantedAuthority("ROLE_" + role.getName()))
                        .collect(Collectors.toSet()));
    }

    @NotNull
    public static JwtUserDetails createFrom(@NotNull ClientUser clientUser) {
        return createFrom(clientUser, null);
    }

}

