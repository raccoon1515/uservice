package org.raccoon.uservice.service.entity;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import javax.persistence.*;
import javax.validation.constraints.Size;

/**
 * Base class for user entities.
 */
@Data
@MappedSuperclass
public abstract class User {

    /**
     * User database id.
     */
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    private long id;

    /**
     * User name.
     */
    @Size(min = 3, max = 15)
    @Column(unique = true, nullable = false, name = "username")
    private String username;

    /**
     * User password.
     */
    @Size(min = 3)
    @JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
    @Column(name = "password", nullable = false)
    private String password;

    /**
     * User enabled flag.
     */
    @Column(nullable = false, name = "enabled")
    private boolean enabled = true;

}
