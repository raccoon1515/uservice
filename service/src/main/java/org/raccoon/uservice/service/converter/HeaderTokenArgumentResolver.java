package org.raccoon.uservice.service.converter;

import org.jetbrains.annotations.NotNull;
import org.raccoon.uservice.service.utils.TokenUtils;
import org.springframework.core.MethodParameter;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.web.bind.support.WebDataBinderFactory;
import org.springframework.web.context.request.NativeWebRequest;
import org.springframework.web.context.request.ServletWebRequest;
import org.springframework.web.method.support.HandlerMethodArgumentResolver;
import org.springframework.web.method.support.ModelAndViewContainer;

import javax.servlet.http.HttpServletRequest;

import java.util.*;

/**
 * Resolves JWT from a header into controllers.
 */
public class HeaderTokenArgumentResolver implements HandlerMethodArgumentResolver {

    @Override
    public boolean supportsParameter(@NotNull MethodParameter parameter) {
        return parameter.getParameterAnnotation(BearerToken.class) != null;
    }

    @Override
    public Object resolveArgument(@NotNull MethodParameter parameter,
                                  ModelAndViewContainer mavContainer,
                                  @NotNull NativeWebRequest webRequest,
                                  WebDataBinderFactory binderFactory) throws Exception {
        final HttpServletRequest request = ((ServletWebRequest) webRequest).getRequest();
        final Optional<String> token = TokenUtils.getNativeToken(request);
        return token.orElseThrow(
                () -> new AccessDeniedException("Invalid bearer token provided"));
    }

}
