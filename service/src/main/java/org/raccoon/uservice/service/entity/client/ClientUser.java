package org.raccoon.uservice.service.entity.client;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.raccoon.uservice.service.converter.ClientUserMetadataDbConverter;
import org.raccoon.uservice.service.entity.User;
import org.raccoon.uservice.service.entity.service.ServiceUser;

import javax.persistence.*;
import java.util.Map;

/**
 * Client user database entity.
 * A bunch "username-service_id" must be unique.
 */
@Entity
@Table(
        name = "client_user",
        uniqueConstraints = @UniqueConstraint(columnNames = {"service_id", "username"}))
@Data
@EqualsAndHashCode(callSuper = true)
public class ClientUser extends User {

    /**
     * Stored {@link ClientUser} metadata.
     */
    @SuppressWarnings("JpaAttributeTypeInspection")
    @Column(columnDefinition = "text", name = "metadata")
    @Convert(converter = ClientUserMetadataDbConverter.class)
    private Map<String, Object> metadata;

    /**
     * Assigned {@link ServiceUser}.
     */
    @JsonIgnore
    @ManyToOne(optional = false, cascade = CascadeType.REMOVE, fetch = FetchType.LAZY)
    @JoinColumn(nullable = false, updatable = false, name = "service_id")
    private ServiceUser service;

}
