package org.raccoon.uservice.service.api.http;

import org.raccoon.uservice.service.consts.MvcEndpoints;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.UrlResource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.io.File;
import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;

/**
 * Allows any to download public key.
 */
@RestController
@RequestMapping(MvcEndpoints.PUBLIC_KEY)
public class PublicKeyController {

    @Value("${keys.public}")
    private String pubKeyPath;

    @GetMapping
    public ResponseEntity<UrlResource> getPublicKey() throws IOException {
        final Path path = Paths.get(pubKeyPath);
        final UrlResource resource = new UrlResource(path.toUri());
        final File file = resource.getFile();

        return ResponseEntity.ok()
                .contentType(MediaType.parseMediaType("application/x-pem-file"))
                .contentLength(file.length())
                .header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\"" + file.getName() + "\"")
                .body(resource);
    }

}
