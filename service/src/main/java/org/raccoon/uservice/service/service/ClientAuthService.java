package org.raccoon.uservice.service.service;

import org.apache.commons.lang3.ObjectUtils;
import org.jetbrains.annotations.NotNull;
import org.raccoon.uservice.service.entity.client.ClientJwt;
import org.raccoon.uservice.service.entity.client.ClientUser;
import org.raccoon.uservice.service.repository.JwtRepository;
import org.raccoon.uservice.service.repository.UserRepository;
import org.raccoon.uservice.service.security.JwtProvider;
import org.raccoon.uservice.service.security.auth.ClientAuthenticationProvider.ClientAuthenticationToken;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.cache.CacheManager;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.stereotype.Service;

import java.util.Map;

/**
 * Client authentication service.
 */
@Service
public class ClientAuthService extends AbstractAuthService<ClientJwt, ClientUser> {

    public ClientAuthService(JwtProvider jwtProvider,
                             JwtRepository<ClientJwt> jwtRepository,
                             CacheManager cacheManager,
                             @Qualifier("authenticationManagerBean") AuthenticationManager authenticationManager,
                             UserRepository<ClientUser> userRepository) {
        super(jwtProvider, jwtRepository, cacheManager, authenticationManager, userRepository);
    }

    @Override
    protected UsernamePasswordAuthenticationToken createAuthToken(String username, String password) {
        return new ClientAuthenticationToken(username, password);
    }

    @Override
    protected ClientJwt createJwt(@NotNull ClientUser user) {
        final ClientJwt jwt = new ClientJwt();
        jwt.setUser(user);
        return jwt;
    }

    @Override
    protected Map<String, Object> getJwtMetadata(@NotNull ClientUser user) {
        return ObjectUtils.defaultIfNull(user.getMetadata(), super.getJwtMetadata(user));
    }

}
