package org.raccoon.uservice.service.api.grpc;

import io.grpc.stub.StreamObserver;
import lombok.extern.slf4j.Slf4j;
import org.lognet.springboot.grpc.GRpcService;
import org.raccoon.uservice.client.grpc.Void;
import org.raccoon.uservice.client.grpc.*;
import org.raccoon.uservice.service.converter.proto.UserProtoConverter;
import org.raccoon.uservice.service.entity.client.ClientUser;
import org.raccoon.uservice.service.service.UserService;

@Slf4j
@GRpcService
public class GrpcUserService extends UserServiceGrpc.UserServiceImplBase {

    private final UserService userService;

    public GrpcUserService(UserService userService) {
        this.userService = userService;
    }

    @Override
    public void getAllUsers(Void request, StreamObserver<UserListProto> responseObserver) {
        log.debug("Requested \"getAllUsers\"");
        final Iterable<ClientUser> users = userService.getAllUsers();

        final UserListProto userListProto = UserProtoConverter
                .convertUserListToProto(users);
        responseObserver.onNext(userListProto);
        responseObserver.onCompleted();
    }

    @Override
    public void getAllRoles(Void request, StreamObserver<RoleListProto> responseObserver) {
//        final RoleListProto roleListProto =
//                UserProtoConverter.convertRoleListToProto(roleService.getAllRoles());
//
//        responseObserver.onNext(roleListProto);
//        responseObserver.onCompleted();
    }

    @Override
    public void getUserById(UserId request, StreamObserver<UserProto> responseObserver) {

    }

    // todo: implement other methods
}
