package org.raccoon.uservice.service.repository;

import org.raccoon.uservice.service.entity.User;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.NoRepositoryBean;

/**
 * Base user repository.
 *
 * @param <T> user type.
 * @see org.raccoon.uservice.service.repository.client.ClientUserRepository
 * @see org.raccoon.uservice.service.repository.service.ServiceUserRepository
 */
@NoRepositoryBean
public interface UserRepository<T extends User> extends CrudRepository<T, Long> {

    T findUserById(long userId);

    T findUserByUsername(String username);

}
