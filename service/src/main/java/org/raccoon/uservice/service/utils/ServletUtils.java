package org.raccoon.uservice.service.utils;

import lombok.experimental.UtilityClass;
import org.jetbrains.annotations.NotNull;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;

import javax.servlet.http.HttpServletResponse;

import java.io.IOException;
import java.nio.charset.StandardCharsets;

/**
 * Servlet utilities.
 */
@UtilityClass
public class ServletUtils {

    /**
     * Writes a message to response.
     *
     * @param messageJson JSON-stringified message
     * @throws IOException if an input or output exception occurred.
     */
    public static void sendJsonMessage(@NotNull HttpServletResponse response,
                                       @NotNull HttpStatus httpStatus,
                                       @NotNull String messageJson) throws IOException {

        response.setStatus(httpStatus.value());
        response.setContentType(MediaType.APPLICATION_JSON.toString());
        response.setCharacterEncoding(StandardCharsets.UTF_8.name());
        response.getWriter().write(messageJson);
    }

}
