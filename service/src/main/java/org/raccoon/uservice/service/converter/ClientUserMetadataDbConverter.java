package org.raccoon.uservice.service.converter;

import com.fasterxml.jackson.core.JsonProcessingException;
import org.raccoon.uservice.service.utils.JsonUtils;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;
import java.util.Map;

/**
 * Converts {@link org.raccoon.uservice.service.entity.client.ClientUser}
 * {@code metadata} to JSON-string for a db storing & to Map for entity attribute.
 *
 * @see org.raccoon.uservice.service.entity.client.ClientUser
 */
@Converter(autoApply = true)
public class ClientUserMetadataDbConverter implements AttributeConverter<Map<String, Object>, String> {

    @Override
    public String convertToDatabaseColumn(Map<String, Object> attribute) {
        try {
            return JsonUtils.convertToJson(attribute);
        } catch (JsonProcessingException e) {
            throw new RuntimeException("Failed to convert client user metadata to db column");
        }
    }

    @Override
    @SuppressWarnings("unchecked")
    public Map<String, Object> convertToEntityAttribute(String dbData) {
        try {
            return JsonUtils.readFrom(dbData, Map.class);
        } catch (JsonProcessingException e) {
            throw new RuntimeException("Failed to convert client user metadata db column to map");
        }
    }

}
