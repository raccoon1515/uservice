package org.raccoon.uservice.service.entity.service;

import lombok.Data;

import javax.persistence.*;

/**
 * Service user role database entity.
 *
 * @see ServiceUser
 */
@Entity
@Table(name = "service_role")
@Data
public class Role  {

    /**
     * Role database id.
     */
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    private long id;

    /**
     * Name of role.
     */
    @Column(nullable = false, unique = true, name = "name")
    private String name;
}
