package org.raccoon.uservice.service.repository.service;

import org.raccoon.uservice.service.entity.service.ServiceJwt;
import org.raccoon.uservice.service.repository.JwtRepository;
import org.springframework.stereotype.Repository;

/**
 * Service JWT repository.
 */
@Repository
public interface ServiceJwtRepository extends JwtRepository<ServiceJwt> {

}
