package org.raccoon.uservice.service.security;

import io.jsonwebtoken.*;
import lombok.extern.slf4j.Slf4j;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.raccoon.uservice.service.dto.TokenPair;
import org.raccoon.uservice.service.security.auth.ClientAuthenticationProvider.ClientAuthenticationToken;
import org.raccoon.uservice.service.security.auth.ServiceAuthenticationProvider.ServiceAuthenticationToken;
import org.raccoon.uservice.service.security.details.ClientJwtUserDetailsService;
import org.raccoon.uservice.service.security.details.ServiceJwtUserDetailsService;
import org.raccoon.uservice.service.utils.PemUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.util.*;

@Slf4j
@Component
public class JwtProvider {

    private final ServiceJwtUserDetailsService serviceJwtUserDetailsService;
    private final ClientJwtUserDetailsService clientJwtUserDetailsService;
    private final JwtParser parser;

    private PrivateKey privateKey;

    @Value("${keys.private}")
    private String privateKeyPath;

    @Value("${keys.public}")
    private String publicKeyPath;

    @Value("${token.lifetime.access:15}")
    private int accessTokenLifetime;

    @Value("${token.lifetime.refresh:43200}")
    private int refreshTokenLifetime;


    public JwtProvider(ServiceJwtUserDetailsService serviceJwtUserDetailsService,
                       ClientJwtUserDetailsService clientJwtUserDetailsService) {
        this.serviceJwtUserDetailsService = serviceJwtUserDetailsService;
        this.clientJwtUserDetailsService = clientJwtUserDetailsService;
        this.parser = Jwts.parser();
    }

    @NotNull
    public TokenPair createTokenPair(@NotNull String username) {
        return createTokenPair(username, null);
    }

    @NotNull
    public TokenPair createTokenPair(@NotNull String username, @Nullable Map<String, ?> payload) {
        return new TokenPair(
                createAccessToken(username, payload),
                createRefreshToken(username, null)
        );
    }

    @NotNull
    public String createAccessToken(@NotNull String username, @Nullable Map<String, ?> payload) {
        return createToken(username, accessTokenLifetime, payload);
    }

    @NotNull
    public String createRefreshToken(@NotNull String username, @Nullable Map<String, ?> payload) {
        return createToken(username, refreshTokenLifetime, payload);
    }

    public void validateToken(@NotNull String refreshToken) {
        parser.parseClaimsJws(refreshToken);
    }

    @NotNull
    public Authentication getServiceAuthentication(@NotNull String token) {
        final String username = getUsername(token);
        final UserDetails userDetails = serviceJwtUserDetailsService.loadUserByUsername(username);
        return new ServiceAuthenticationToken(userDetails, "", userDetails.getAuthorities());
    }

    @NotNull
    public Authentication getClientAuthentication(@NotNull String token) {
        final String username = getUsername(token);
        final UserDetails userDetails = clientJwtUserDetailsService.loadUserByUsername(username);
        return new ClientAuthenticationToken(userDetails, "");
    }

    @NotNull
    public String getUsername(@NotNull String token) {
        return parser.parseClaimsJws(token).getBody().getSubject();
    }

    @NotNull
    private String createToken(String username, int lifetime, Map<String, ?> payload) {
        final Claims claims = Jwts.claims().setSubject(username);

        if (payload != null) {
            claims.putAll(payload);
        }

        final Date now = new Date();
        return Jwts.builder()
                .setClaims(claims)
                .setIssuedAt(now)
                .setIssuer("Uservice")
                .setExpiration(new Date(System.currentTimeMillis() + lifetime * 60_000L))
                .signWith(privateKey, SignatureAlgorithm.RS256)
                .compact();
    }

    @PostConstruct
    private void init() throws IOException {
        final String KEYS_ALGORITHM = "RSA";
        try {
            final PublicKey publicKey = PemUtils.readPublicKeyFromFile(publicKeyPath, KEYS_ALGORITHM);
            this.privateKey = PemUtils.readPrivateKeyFromFile(privateKeyPath, KEYS_ALGORITHM);
            this.parser.setSigningKey(publicKey);
        } catch (IOException e) {
            if (e instanceof FileNotFoundException) {
                log.warn(KEYS_ALGORITHM + " key not found, cause: " + e.getMessage());
            }
            throw e;
        }
    }

}
