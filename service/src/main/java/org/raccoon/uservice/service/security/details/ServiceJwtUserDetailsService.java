package org.raccoon.uservice.service.security.details;


import org.raccoon.uservice.service.entity.service.ServiceUser;
import org.raccoon.uservice.service.repository.service.ServiceUserRepository;
import org.raccoon.uservice.service.security.JwtUserDetailsFactory;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

@Service
public class ServiceJwtUserDetailsService implements UserDetailsService {

    private final ServiceUserRepository serviceUserRepository;

    public ServiceJwtUserDetailsService(ServiceUserRepository serviceUserRepository) {
        this.serviceUserRepository = serviceUserRepository;
    }

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        final ServiceUser serviceUser = serviceUserRepository.findUserByUsername(username);
        if (serviceUser == null) {
            throw new UsernameNotFoundException("Username " + username + " not found");
        }
        return JwtUserDetailsFactory.createFrom(serviceUser, serviceUser.getRoles());
    }

}
