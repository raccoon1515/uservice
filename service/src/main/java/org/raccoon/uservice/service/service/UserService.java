package org.raccoon.uservice.service.service;

import org.apache.commons.lang3.ObjectUtils;
import org.apache.commons.lang3.StringUtils;
import org.jetbrains.annotations.NotNull;
import org.raccoon.uservice.service.consts.CacheNames;
import org.raccoon.uservice.service.entity.client.ClientUser;
import org.raccoon.uservice.service.entity.service.ServiceUser;
import org.raccoon.uservice.service.exception.ResourceNotFoundException;
import org.raccoon.uservice.service.exception.UserviceException;
import org.raccoon.uservice.service.repository.client.ClientJwtRepository;
import org.raccoon.uservice.service.repository.client.ClientUserRepository;
import org.raccoon.uservice.service.repository.service.ServiceUserRepository;
import org.springframework.cache.annotation.*;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;

import java.util.*;

/**
 * Service provides CRUD operations on {@link ClientUser}.
 */
@Service
@CacheConfig(cacheNames = CacheNames.CLIENT_USERS)
public class UserService {

    private final ClientUserRepository userRepository;
    private final ServiceUserRepository serviceRepository;
    private final ClientJwtRepository jwtRepository;
    private final PasswordEncoder passwordEncoder;

    public UserService(ClientUserRepository userRepository,
                       ServiceUserRepository serviceRepository,
                       ClientJwtRepository jwtRepository,
                       PasswordEncoder passwordEncoder) {
        this.userRepository = userRepository;
        this.serviceRepository = serviceRepository;
        this.jwtRepository = jwtRepository;
        this.passwordEncoder = passwordEncoder;
    }

    /**
     * Creates and stores new {@link ClientUser}.
     *
     * @param user the user to be created and stored
     * @param serviceName a {@link ServiceUser} name to be assigned to user
     * @throws UserviceException if user with such a username and service name already exist.
     * @return created {@link ClientUser}
     */
    @NotNull
    @CachePut(key = "#user.id")
    public ClientUser createUser(@NotNull ClientUser user, @NotNull String serviceName) {
        final ServiceUser service = getServiceUser(serviceName);

        user.setPassword(passwordEncoder.encode(user.getPassword()));
        user.setService(service);

        try {
            return userRepository.save(user);
        } catch (Exception e) {
            throw new UserviceException("Username " + user.getUsername() + " already exists");
        }
    }

    /**
     * Gets all stored users.
     *
     * @deprecated Service shouldn't allow to get all users.
     */
    @NotNull
    @Deprecated
    public Iterable<ClientUser> getAllUsers() {
        return userRepository.findAll();
    }

    /**
     * Gets {@link ClientUser} assigned to {@link ServiceUser}.
     *
     * @param userId an {@link ClientUser} id
     * @param serviceName a {@link ServiceUser} name.
     * @return stored {@link ClientUser}
     */
    @NotNull
    @Cacheable(key = "#userId")
    public ClientUser getUserById(long userId, @NotNull String serviceName) {
        return getClientUser(userId, serviceName);
    }

    /**
     * Updates user by provided {@link ClientUser} values.
     * New must contain an id.
     * If {@code metadata} field provided, then original metadata will be rewrited.
     *
     * @param newUserValues a values to be updated
     * @param serviceName a {@link ServiceUser} name assigned to user.
     * @return updated {@link ClientUser}
     */
    @NotNull
    @CachePut(key = "#newUserValues.id")
    public ClientUser updateUser(@NotNull ClientUser newUserValues, @NotNull String serviceName) {
        final ClientUser dbUser = getClientUser(newUserValues.getId(), serviceName);

        if (dbUser.equals(newUserValues)) {
            return dbUser;
        }

        final String password = newUserValues.getPassword();
        if (StringUtils.isNotBlank(password)) {
            dbUser.setPassword(passwordEncoder.encode(password));
        }
        dbUser.setUsername(ObjectUtils.defaultIfNull(newUserValues.getUsername(), dbUser.getUsername()));
        dbUser.setEnabled(ObjectUtils.defaultIfNull(newUserValues.isEnabled(), dbUser.isEnabled()));
        dbUser.setMetadata(ObjectUtils.defaultIfNull(newUserValues.getMetadata(), dbUser.getMetadata()));
        return userRepository.save(dbUser);
    }

    /**
     * Deletes {@link ClientUser} by its id.
     *
     * @param userId a {@link ClientUser} id
     * @param serviceName a {@link ServiceUser} name.
     */
    @Transactional
    @CacheEvict(key = "#userId")
    public void deleteUser(long userId, @NotNull String serviceName) {
        final ClientUser user = getClientUser(userId, serviceName);

        jwtRepository.deleteClientJwtByUserId(user.getId());
        userRepository.deleteById(user.getId());
    }

    /**
     * Returns persisted {@link ClientUser} from db.
     *
     * @throws ResourceNotFoundException if user with such an id doesn't exist.
     */
    @NotNull
    private ClientUser getClientUser(long userId, @NotNull String serviceName) {
        final ServiceUser service = getServiceUser(serviceName);

        final Optional<ClientUser> user = userRepository.findClientUserByIdAndServiceId(userId, service.getId());
        return user.orElseThrow(
                () -> new ResourceNotFoundException("User with id " + userId + " doesn't exist"));
    }

    /**
     * Returns persisted {@link ServiceUser} from db.
     *
     * @throws UserviceException if service with such a name doesn't exist.
     */
    @NotNull
    private ServiceUser getServiceUser(@NotNull String serviceName) {
        final ServiceUser service = serviceRepository.findUserByUsername(serviceName);
        if (service == null) {
            throw new UserviceException("Service with name " + serviceName + " doesn't exist");
        }
        return service;
    }

}
