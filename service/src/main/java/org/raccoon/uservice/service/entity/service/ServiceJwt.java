package org.raccoon.uservice.service.entity.service;

import lombok.Data;
import lombok.EqualsAndHashCode;
import org.raccoon.uservice.service.entity.Jwt;
import org.raccoon.uservice.service.entity.service.ServiceUser;

import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

/**
 * Service JWT database entity.
 */
@EqualsAndHashCode(callSuper = true)
@Data
@Entity
@Table(name = "service_jwt")
public class ServiceJwt extends Jwt {

    /**
     * Assigned {@link ServiceUser}.
     */
    @OneToOne(optional = false)
    @JoinColumn(name = "user_id")
    private ServiceUser user;

}
