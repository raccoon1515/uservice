create table service_role
(
    id   bigint primary key  not null,
    name varchar(255) unique not null
) engine = InnoDB;

create table service_user
(
    id       bigint primary key not null,
    enabled  bit                not null,
    password varchar(255)       not null,
    username varchar(15) unique not null
) engine = InnoDB;

create table service_users_role
(
    user_id bigint not null,
    role_id bigint not null,
    constraint foreign key (user_id) references service_user (id),
    constraint foreign key (role_id) references service_role (id)
) engine = InnoDB;

create table client_user
(
    id         bigint primary key not null,
    enabled    bit                not null,
    password   varchar(255)       not null,
    username   varchar(15)        not null,
    metadata   text,
    service_id bigint             not null,
    constraint foreign key (service_id) references service_user (id),
    constraint unique_username_service_id unique (service_id, username)
) engine = InnoDB;

create table client_jwt
(
    id      bigint primary key not null,
    token   text               not null,
    user_id bigint             not null,
    constraint foreign key (user_id) references client_user (id)
) engine = InnoDB;

create table service_jwt
(
    id      bigint primary key not null,
    token   text               not null,
    user_id bigint             not null,
    constraint foreign key (user_id) references service_user (id)
) engine = InnoDB;

create table hibernate_sequence
(
    next_val bigint
) engine = InnoDB;

insert into service_role (id, name)
values (1, 'admin');

insert into service_user (id, enabled, username, password)
values (1, TRUE, 'admin', '$2y$04$yT2JvZuLOUd65wI5ubbcseSZQWhAcH3lVFWMT2OqyJ3ViZLrB9p5O');

insert into service_users_role
values (1, 1);

insert into hibernate_sequence
values (3);
